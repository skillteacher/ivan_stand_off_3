using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


[RequireComponent(typeof(Data))]
public class Dvizenievraga : MonoBehaviour
{
    [SerializeField] private float visionRange = 10f;
    private Data data;  



    private void Awake()
    { 
       data = GetComponent<Data>();
    }



    private void Update()
    {
        if (IsTargetInVisionRange())
        {
            if(data.distanceToTarget > data.agent.stoppingDistance)
            {
                data.animator.SetBool("run", true);
            }
            else
            {
                data.animator.SetBool("run", false);
            }
            data.agent.SetDestination(data.target.position);
            
        }
    }



    private bool IsTargetInVisionRange()
    {
        
        return visionRange > data.distanceToTarget;
    }

    //private void OnDrawGizmos()
    //{
    //    Gizmos.DrawWireSphere(transform.position, visionRange);
    //}
}











