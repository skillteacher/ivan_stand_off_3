using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] private float starthitpoints = 100f;
    [SerializeField] private bool isPlayer;
    private float hitpoints;

    private void Start()
    {
        hitpoints = starthitpoints;
    }

    public void TakeDamage(float damage)
    {
        hitpoints -= damage;
        if(hitpoints < 0f)
        {           
            if (isPlayer) FindAnyObjectByType<Game>().GameOver();
            else Destroy(gameObject);
        }

    }

}


