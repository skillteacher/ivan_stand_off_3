using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Data))]
public class Otakavraga : MonoBehaviour
{
    [SerializeField] private float damage = 10f;
    [SerializeField] private float range = 2f;
    private Data data;

    private void Awake()
    {
        data = GetComponent<Data>();
    }

    private void Update()
    {
        data.animator.SetBool("attack", data.distanceToTarget > range ? false : true);
    }

    public void AttackTarget()
    {
        data.targetHealth.TakeDamage(damage);
    }
}
















