using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AmmoPikUp : MonoBehaviour
{
    [SerializeField] private int ammoAmount = 2;
    [SerializeField] private TextMeshProUGUI ammoText;
}
