using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimation : MonoBehaviour
{
    private Otakavraga enemyAttack;
    
    private void Awake()
    {
        enemyAttack = GetComponentInParent<Otakavraga>();
    }
    public void AttackMoment()
    {
        enemyAttack.AttackTarget();
    }
}
