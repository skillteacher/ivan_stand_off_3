using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] private Transform cameraTranform;
    [SerializeField] private float damage = 10f;
    [SerializeField] private float range = 100f;
    [SerializeField] private ParticleSystem muzzleFlash;
    [SerializeField] private GameObject sparksPefab;
    [SerializeField] private float sparksLivetime = 2000f;

    private void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
            PlayEffect();
        }
    }

    private void Shoot()
    {
        RaycastHit hit;
        if (!Physics.Raycast(cameraTranform.position, cameraTranform.forward, out hit, range)) return;
        HitEffect(hit.point);
        Health health = hit.transform.GetComponent<Health>();
        if (health == null) return;
        health.TakeDamage(damage);

    }

    private void PlayEffect()
    {
        muzzleFlash.Play();
    }

    private void HitEffect(Vector3 point)
    {
        GameObject sparks;
        sparks = Instantiate(sparksPefab, point, Quaternion.identity);
        Destroy(sparks, sparksLivetime);

    }
}
